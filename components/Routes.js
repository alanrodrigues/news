import Blogs from './Blogs';
import Post from './Post';
import Edit from './Edit';

import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';


const NavStack = createStackNavigator({
    Blogs: {
        screen: Blogs,
        navigationOptions: () => ({
            headerTitle: "Notícias"
        })
    },
    Edit: {
        screen: Edit,
        navigationOptions: () => ({
            headerTitle: "Editar"
        })
    },
}, {
    //headerLayoutPreset
    headerTitleAlign: "center"
})

const BottomTab = createBottomTabNavigator({
    Notícias: {
        screen: NavStack
    },
    Posts: {
        screen: Post
    }
})

export default Routes = createAppContainer(BottomTab)